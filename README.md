Retina LCD Driver v1.1 2018
==========================================
LP097QX1 (SP)(AV) driver. Based on Adafruit project.
Features:
- TPS61176 as a backlight driver
- 0.6 mm PCB thickness to achieve 100R differential pairs impedance in 2 layers board
- DisplayPort connector

![Rendering][rendering1]

[rendering1]: ./retina1.jpg

![Rendering][rendering]

[rendering]: ./retina2.jpg



